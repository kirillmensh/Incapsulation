

#include <iostream>
#include <math.h>

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Set(double _x, double _y, double _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    double Lenght()
    {
        return sqrt(x*x + y*y + z*z);
    }
    void Show()
    {
        std::cout << " x: " << x <<" y: "<<y<<" z: "<<z;
    }
private:
    double x;
    double y;
    double z;
};


int main()
{
    Vector vector;
    std::cout << '\n';
    vector.Set(1, 2, 3);
    vector.Show();
    
    std::cout << '\n'<< vector.Lenght() << '\n';
}

